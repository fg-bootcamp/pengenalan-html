const modal = document.querySelector(".modal");
const showModal = document.querySelector(".btn-modal");
const cancelModal = document.querySelector(".btn-cancel");
const submitBtn = document.querySelector(".btn-submit");

showModal.addEventListener("click", triggerModall);
cancelModal.addEventListener("click", triggerModall);

submitBtn.addEventListener("click", function submitForm(e) {
  e.preventDefault();
  alert("Form was submitted succesfully");
});

function triggerModall() {
  modal.classList.toggle("active");
}
