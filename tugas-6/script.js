const content = document.getElementsByClassName("content")[0];

content.style.background = "lightblue";
content.style.padding = "50px";

const title = document.getElementsByTagName("h1")[0];

title.innerText = "JUDUL 1";
title.style.textAlign = "center";

const paragraphs = document.getElementsByTagName("p");

for (const p of paragraphs) {
  p.innerText = "Bukan Lorem Lagi";
  p.style.color = "#666";
}

const list = document.querySelectorAll("li");

for (let i = 0; i < list.length; i++) {
  list[i].innerText = `List ${i + 1}`;

  if (i === 0) {
    list[i].style.color = "royalblue";
  } else if (i === 1) {
    list[i].style.color = "#111";
  } else if (i === 2) {
    list[i].style.color = "aqua";
  } else if (i === 3) {
    list[i].style.color = "red";
  } else {
    list[i].style.color = "gray";
  }
}
