const body = document.querySelector("body");
body.style.fontFamily = "Arial, Helvetica, sans-serif";
const title = document.querySelector("h1");
const paragraph = document.createElement("p");
paragraph.innerHTML = `Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa, natus qui voluptatum alias doloribus amet explicabo sequi repellat reiciendis nostrum iure reprehenderit labore, exercitationem tempora dolores magnam animi minima quod sint quae quaerat tempore fugiat. Iusto tempore corporis saepe dolore?Lorem ipsum, dolor sit amet consectetur adipisicing elit. A, porro deserunt, distinctio eum molestiae nihil, magni ut quasi tenetur impedit consequuntur sunt dolorem iure harum.`;
title.after(paragraph);

const image = document.createElement("img");
image.setAttribute("src", "./school.jpg");
image.setAttribute("width", "400px");

paragraph.after(image);
const title2 = document.createElement("h2");
title2.innerHTML = "Our Partners";
image.after(title2);

const partner = ["Facebook", "Instagram", "Youtube"];
const ul = document.createElement("ul");
for (const val of partner) {
  const anchor = document.createElement("a");
  const li = document.createElement("li");
  anchor.innerHTML = val;
  if (val === "Facebook") {
    anchor.setAttribute("href", "https://facebook.com");
    anchor.style.color = "red";
  } else if (val === "Instagram") {
    anchor.setAttribute("href", "https://instagram.com");
  } else {
    anchor.setAttribute("href", "https://youtube.com");
    anchor.style.color = "green";
  }
  li.append(anchor);
  ul.append(li);
}
title2.after(ul);

const contactTitle = document.createElement("h3");
contactTitle.innerHTML = "Contact Us";
contactTitle.style.fontWeight = "normal";
contactTitle.style.fontSize = "32px";

ul.after(contactTitle);

const form = document.createElement("form");
const contact = ["Name", "Email", "Phone"];

for (const con of contact) {
  const labelContainer = document.createElement("div");
  const label = document.createElement("label");
  const input = document.createElement("input");
  label.style.marginRight = "15px";
  label.innerHTML = con;
  labelContainer.append(label);
  labelContainer.append(input);
  form.append(labelContainer);
}
const btn = ["Reset", "Submit"];

for (const b of btn) {
  const button = document.createElement("button");
  button.innerHTML = b;
  button.style.padding = "7px 15px";
  button.style.borderRadius = "5px";
  button.style.marginTop = "15px";
  button.style.border = "1px solid #444";

  if (b === "Submit") {
    button.style.background = "blue";
    button.style.marginLeft = "15px";
  }
  form.append(button);
}

contactTitle.after(form);
